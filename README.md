# Terraform README

This README provides an overview and explanation of the Terraform code in this repository.

## Table of Contents

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Usage](#usage)

## Introduction

In this repository, you will find Terraform code that provisions and manages infrastructure resources. The code is organized into modules, each responsible for a specific component or resource.

## Prerequisites

Before using this Terraform code, ensure that you have the following prerequisites:

- Terraform installed on your local machine
- Puppetmaster running either in VM or AWS EC2 instance
- Access to the cloud provider (e.g., AWS, Azure) where the infrastructure will be provisioned
- Proper authentication and access credentials for the cloud provider

## Getting Started

To get started with this Terraform code, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the appropriate directory containing the Terraform code.
3. Initialize the Terraform working directory by running `terraform init`.
4. Review the execution plan by running `terraform plan`.
5. Apply the changes by running `terraform apply`.
6. Meanwhile paste the init.pp file under the manifests directory of the puppet server.
7. After step 5 gets completed, sign the puppet agent certificate from the puppet master instance.

## Usage

The terraform code will provision a new EC2 instance and automate the installation of Puppet Agent in it. After completing the installation, Puppet Agent service will be started and apply the catalog from Puppet-Server.

## Note

Puppet Server needs to manually created.


## Sample OP Screenshot 
![OP Screenshot](/terraform_ss.png)
